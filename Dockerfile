FROM ruby:alpine
RUN apk add --update build-base tzdata nodejs
RUN gem install bundler

WORKDIR /rails-app
COPY Gemfile /rails-app/Gemfile
COPY Gemfile.lock /rails-app/Gemfile.lock
# RUN gem update bundler
RUN bundle install 
ADD . /rails-app
# COPY docker-entrypoint.sh /usr/local/bin
# ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 3000
CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]